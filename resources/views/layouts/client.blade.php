<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from html.gridbootstrap.com/charityo/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 18 Mar 2019 13:30:51 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Theme Region">
    <meta name="description" content="">

    <title>Template</title>

    <!-- CSS -->
    <link rel="stylesheet" href="temp/css/structure.css">
    <link rel="stylesheet" href="temp/css/slick.css">
    <link rel="stylesheet" href="temp/css/main.css">
    <link id="preset" rel="stylesheet" href="temp/css/presets/preset5.css">
    <link rel="stylesheet" href="temp/css/responsive.css">
    <!-- Profilleee -->




    <!-- Profilleee -->
    <link rel="stylesheet" href="temp/css/bootstrap.min.css" >
    <link rel="stylesheet" href="temp/css/font-awesome.min.css">
    <link rel="stylesheet" href="temp/css/magnific-popup.css">
    <link rel="stylesheet" href="temp/css/YTPlayer.min.css">
    {{--<link rel="stylesheet" href="temp/css/animate.css">--}}
    <link rel="stylesheet" href="temp/css/structure.css">
    <link rel="stylesheet" href="temp/css/slick.css">
    <link rel="stylesheet" href="temp/css/main.css">
    <link id="preset" rel="stylesheet" href="temp/css/presets/preset5.css">
    <link rel="stylesheet" href="temp/css/responsive.css">
    <!-- font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <!-- icons -->
    <link rel="icon" href="temp/images/ico/favicon.ico">
    <link rel="apple-touch-icon" sizes="144x144" href="https://html.gridbootstrap.com/images/ico/apple-touch-icon-precomposed.png">
    <link rel="apple-touch-icon" sizes="114x114" href="https://html.gridbootstrap.com/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon" sizes="72x72" href="https://html.gridbootstrap.com/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon" sizes="57x57" href="https://html.gridbootstrap.com/images/ico/apple-touch-icon-57-precomposed.png">
    <!-- icons -->
    <meta name="csrf-token" value="{{ csrf_token() }}" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Template Developed By ThemeRegion -->
</head>

<body >
        <div id="app">
            <Accueil></Accueil>

        </div>



    <!-- JS -->
    {{--<script src="temp/js/jquery.min.js"></script>--}}
    {{--<script src="temp/js/popper.min.js"></script>--}}
    {{--<script src="temp/js/bootstrap.min.js"></script>--}}
    {{--<script src="temp/js/jquery.nav.js"></script>--}}
    {{--<script src="temp/js/masonry.pkgd.min.js"></script>--}}
    {{--<script src="temp/js/magnific-popup.min.js"></script>--}}
    {{--<script src="temp/js/counterup.min.js"></script>--}}
    {{--<script src="temp/js/inview.min.js"></script>--}}
    {{--<script src="temp/js/waypoints.min.js"></script>--}}
    {{--<script src="temp/js/slick.min.js"></script>--}}
    {{--<script src="temp/js/jarallax.min.js"></script>--}}
    {{--<script src="temp/js/YTPlayer.min.js"></script>--}}
    {{--<script src="temp/js/switcher.js"></script>--}}
    {{--<script src="temp/js/main.js"></script>--}}


    <script src="js/app.js" type="text/javascript"></script>
</body>

<!-- Mirrored from html.gridbootstrap.com/charityo/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 18 Mar 2019 13:32:42 GMT -->
</html>