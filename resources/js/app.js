/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import Login from "./components/auth/Login";


require('./bootstrap');

window.Vue = require('vue');

import Vue from 'vue'

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);



import VueSweetalert2 from 'vue-sweetalert2';

Vue.use(VueSweetalert2);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);


import Accueil from './Components/Accueil'
import register from "./components/auth/register";
import Header from "./components/Header";
Vue.component('Header', Header);
import Comite from "./components/Comite";
import Concomite from "./components/Concomite";

import ContentAcceuil from "./components/ContentAcceuil";
Vue.component('ContentAcceuil', ContentAcceuil);

import Batir from "./components/service/batir/Batir";
import Editbatir from "./components/service/batir/Editbatir";
import Listebatir from "./components/service/batir/Listebatir";



import STgeo from "./components/Presentation/Vile/STgeo";

import Guide from "./components/service/Guide/Guide";
import Adminurbaine from "./components/service/Guide/Adminurbaine";
import Affadmin from "./components/service/Guide/Affadmin";
import Affeco from "./components/service/Guide/Affeco";
import taxe from "./components/service/Guide/taxe";

import Naissance from "./components/service/Naissance/Naissance";
import AddNaiss from "./components/service/Naissance/AddNaiss";
import EditNaiss from "./components/service/Naissance/EditNaiss";



import Vocation from "./components/service/vocation/Vocation";
import Addvocation from "./components/service/vocation/Addvocation";
import Editvocation from "./components/service/vocation/Editvocation";


import Propriete from "./components/service/Propriete/Propriete";
import AddProp from "./components/service/Propriete/AddProp";
import EditProp from "./components/service/Propriete/EditProp";

import AddEvent from "./components/admin/Events/AddEvent";
import EditEvent from "./components/admin/Events/EditEvent";
import Event from "./components/admin/Events/Event";
import ExtraitNaiss from "./components/service/Naissance/AddNaiss";



import AddPub from "./components/admin/Publication/AddPub";
import ListePub from "./components/admin/Publication/ListePub";
import EditPub from "./components/admin/Publication/EditPub";
import DetailPub from "./components/admin/Publication/DetailPub";


import Dashboard from "./components/admin/Dashboard";



import ListeUsers from "./components/admin/Users/ListeUsers";
import EditUser from "./components/admin/Users/EditUser";

import ConsulterRec from "./components/Client/ConsulterRec";
import Reclamation from "./components/Client/Reclamation";
import Detailrec from "./components/Client/Detailrec";

import ExampleComponent from "./components/ExampleComponent";

import Contact from "./components/Forum/Contact";
import ListeContact from "./components/Forum/ListeContact";



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


let routes = [

    {
        name: 'ContentAcceuil',
        path: '/ContentAcceuil',
        component: ContentAcceuil
    },
    {
        name: 'STgeo',
        path: '/STgeo',
        component: STgeo
    },
    {
        name: 'Accueil',
        path: '/Accueil',
        component: Accueil,
        meta: {
            forAuth: true
        }
    },
    {
        name: 'batir',
        path: '/batir',
        component: Batir,
        meta: {
            forAuth: true
        }
    },

    {
        name: 'Affadmin',
        path: '/Affadmin',
        component: Affadmin,
        meta: {
            forAuth: true
        }
    },
    {
        name: 'Affeco',
        path: '/Affeco',
        component: Affeco,
        meta: {
            forAuth: true
        }
    },
    {
        name: 'taxe',
        path: '/taxe',
        component: taxe,
        meta: {
            forAuth: true
        }
    },
    {
        name: 'Listebatir',
        path: '/Listebatir',
        component: Listebatir,
        meta: {
            forAuth: true
        }
    },
    {
        name: 'Editbatir',
        path: '/Editbatir',
        component: Editbatir,
        meta: {
            forAuth: true
        }
    },
    {
        name: 'EditUser',
        path: '/EditUser',
        component: EditUser,
        meta: {
            forAuth: true
        }
    },
    {
        name: 'Dashboard',
        path: '/Dashboard',
        component: Dashboard,
        meta: {
            forAuth: true
        }
    },

    {
        name: 'ListeUsers',
        path: '/ListeUsers',
        component: ListeUsers,
        meta: {
            forAuth: true
        }
    },
    {
        name: 'ListePub',
        path: '/ListePub',
        component: ListePub,
        meta: {
            forAuth: true
        }
    },
    {
        name: 'DetailPub',
        path: '/DetailPub',
        component: DetailPub,
        meta: {
            forAuth: true
        }
    },
    {
        name: 'AddPub',
        path: '/AddPub',
        component: AddPub,
        meta: {
            forAuth: true
        }
    },
    {
        name: 'EditPub',
        path: '/EditPub',
        component: EditPub,
        meta: {
            forAuth: true
        }
    },

    {
        name: 'Naissance',
        path: '/Naissance',
        component: Naissance,
        meta: {
            forAuth: true
        }
    },
    {
        name: 'AddNaiss',
        path: '/AddNaiss ',
        component: AddNaiss,
        meta: {
            forAuth: true
        }
    },


    {
        name: 'EditNaiss',
        path: '/EditNaiss',
        component: EditNaiss,
        meta: {
            forAuth: true
        }
    },
    {
        name: 'Vocation',
        path: '/Vocation',
        component: Vocation,
        meta: {
            forAuth: true
        }
    },
    {
        name: 'Addvocation',
        path: '/Addvocation',
        component: Addvocation,
        meta: {
            forAuth: true
        }
    },

    {
        name: 'Editvocation',
        path: '/Editvocation',
        component: Editvocation,
        meta: {
            forAuth: true
        }
    },

    {
        name: 'Propriete',
        path: '/Propriete',
        component: Propriete,
        meta: {
            forAuth: true
        }
    },
    {
        name: 'EditProp',
        path: '/EditProp',
        component: EditProp,
        meta: {
            forAuth: true
        }
    },
    {
        name: 'AddProp',
        path: '/AddProp',
        component: AddProp,
        meta: {
            forAuth: true
        }
    },
    {
        name: 'Event',
        path: '/Event',
        component: Event,
        meta: {
            forAuth: true
        }
    },
    {
        name: 'AddEvent',
        path: '/AddEvent',
        component: AddEvent,
        meta: {
            forAuth: true
        }
    },
    {
        name: 'EditEvent',
        path: '/EditEvent',
        component: EditEvent,
        meta: {
            forAuth: true
        }
    },

    {
        name: 'ExtraitNaiss',
        path: '/ExtraitNaiss',
        component: ExtraitNaiss,
        meta: {
            forAuth: true
        }
    },

    {
        name: 'Reclamation',
        path: '/Reclamation',
        component: Reclamation,
        meta: {
            forAuth: true
        }
    },
    {
        name: 'ConsulterRec',
        path: '/ConsulterRec',
        component: ConsulterRec,
        meta: {
            forAuth: true
        }
    },
    {
        name: 'Detailrec',
        path: '/Detailrec',
        component: Detailrec,
        meta: {
            forAuth: true
        }
    },

    {
        name: 'Comite',
        path: '/Comite',
        component: Comite,
        meta: {
            forAuth: true
        }
    },
    {
        name: 'Concomite',
        path: '/Concomite',
        component: Concomite,
        meta: {
            forAuth: true
        }
    },
    {
        name: 'Guide',
        path: '/Guide',
        component: Guide,
        meta: {
            forAuth: true
        }
    },
    {
        name: 'Adminurbaine',
        path: '/Adminurbaine',
        component: Adminurbaine
    },

    {
        name: 'Login',
        path: '/Login',
        component: Login,
        meta: {
            forVisitors: true
        }
    },
    {
        name: 'register',
        path: '/register',
        component: register,
        meta: {
            forVisitors: true
        }
    },
    {
        name: 'ListeContact',
        path: '/ListeContact',
        component: ListeContact,
        meta: {
            forAuth: true
        }

    },
    {
        name: 'Contact',
        path: '/Contact',
        component: Contact
    }

];
const router = new VueRouter({ mode: 'history', routes: routes });


router.beforeEach(
    (to, from, next) => {
        if (to.matched.some(record => record.meta.forVisitors)) {
            if (localStorage.getItem('Token') !== null) {
                next({
                    path: '/ContentAcceuil'
                })
            } else {
                next()
            }
        } else if (to.matched.some(record => record.meta.forAuth)) {
            if (localStorage.getItem('Token') === null) {
                next({
                    path: '/Login'
                })
            } else {
                next()
            }
        }
        else next()

    }
)

const app = new Vue({
    router,
    components: { Accueil, ExampleComponent, Header },
    el: '#app'
});
