<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class actualite extends Model
{
    protected $fillable = [
        'titre','description', 'file','admin_id',
    ];

    public function admins(){
        return $this->belongsTo(admin::class,'admin_id');
    }



}