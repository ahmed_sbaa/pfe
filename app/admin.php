<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class admin extends User
{
    protected $fillable = [
        'matricule','nom','prenom','email','tel'
    ];
    public function actualites(){
        return $this->hasMany(actualite::class);
    }
    public function events(){
        return $this->hasMany(event::class);
    }
    public function contacts(){
        return $this->hasMany(contact::class);
    }
}
