<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class reclamation extends Model
{
    protected $fillable = [
        'sujet','contenu', 'file','image'
    ];

    public function citoyen(){
        return $this->belongsTo(citoyen::class,'citoyen_id');
    }
    public function comites(){
        return $this->hasMany(comite::class);
    }

}
