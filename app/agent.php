<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class agent extends User
{

    protected $fillable = [
        'matricule','nom','prenom','email','tel',
    ];


    public function comites(){
        return $this->hasMany(comite::class);
    }
}
