<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class contact extends Model
{
    protected $fillable = [
        'sujet_message','contenu', 'email','tel','citoyen_id','admin_id',
    ];

    public function citoyens(){
        return $this->belongsTo(citoyen::class,'citoyen_id');
    }
    public function admins(){
        return $this->belongsTo(citoyen::class,'admin_id');
    }
}
