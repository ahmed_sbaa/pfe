<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class comite extends Model
{

    protected $fillable = [
        'nomComite','agent_id','reclamation_id',
];

    public function agents(){
        return $this->belongsTo(agent::class,'agent_id');
    }
    public function reclamations(){
        return $this->belongsTo(reclamation::class,'reclamation_id');
    }

}
