<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class citoyen extends User
{
    protected $fillable = [
        
        'cin_scan','matricule','nom','prenom','email','tel','profession'
    ];



    public function reclamations(){
        return $this->hasMany(reclamation::class);
    }
    public function servises(){
        return $this->hasMany(service::class);
    }
    public function contacts(){
        return $this->hasMany(contact::class);
    }


}
