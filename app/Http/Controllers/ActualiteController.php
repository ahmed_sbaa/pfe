<?php

namespace App\Http\Controllers;

use App\actualite;
use Illuminate\Http\Request;

class ActualiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {



        $search=$request->search;
        $actualite=actualite::where('titre','LIKE',"%$search%")->with('admins')->orderBy("id")->paginate(6);
        return response()->json($actualite);
    }

    public function count()
    {
        $actualite=actualite::all()->count();
        return response()->json($actualite);
    }

/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'titre'=>'required|min:5',
            'description'=>'required|min:5',
            'file'=> 'required|image|mimes:jpg,jpeg,png,gif'
        ]);

        $fileName = null;
        if (request()->hasFile('file')) {
            $file = request()->file('file');
            $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('./images', $fileName);
        }

        $actualite = new actualite();
        $actualite->description=$request->description;
        $actualite->titre=$request->titre;
        $actualite->file=$fileName;
        $actualite->admin_id=$request->admin_id;
        $actualite->save();
        return response()->json('actualite has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $actualite=actualite::find($id);
        return response()->json($actualite);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $actualite=actualite::find($id);
        return response()->json($actualite);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'titre'=>'required|min:5',
            'description'=>'required|min:5',
            'file'=> 'required|image|mimes:jpg,jpeg,png,gif'

        ]);

        $fileName = null;
        if (request()->hasFile('file')) {
            $file = request()->file('file');
            $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('./images', $fileName);
        }


        $actualite=actualite::find($id);

        $actualite->update(['titre'=>$request->get('titre'),
            'description'=>$request->get('description'),
            'file'=>$fileName

            ]);
        return response()->json("updated successfuly");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $actualite=actualite::find($id);
        $actualite->delete();
        return response()->json("deleted ssuccessfuly");
    }
}
