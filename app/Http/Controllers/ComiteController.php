<?php

namespace App\Http\Controllers;

use App\comite;
use Illuminate\Http\Request;

class ComiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comite=comite::with('agents')->orderBy("id")->paginate(3);
            //->orderBy("id")->paginate(4);
        return response()->json($comite);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nomComite'=>'required|string|min:3',
        ]);
//        $comite = new comite([
//            'nomComite' => $request->get('nomComite'),
//            'agent_id'=> $request->get('agent_id'),
//        ]);
        $comite = new comite();
        $comite->nomComite=$request->nomComite;
        $comite->agent_id=$request->agent_id;
        $comite->save();
        return response()->json('comite has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comite=comite::find($id);
        return response()->json($comite);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comite=comite::find($id);
        return response()->json($comite);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nomComite'=>'required|string|min:3',
        ]);

        $comite=comite::find($id);
        $comite->update($request->all());
        return response()->json("updated successfuly");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comite=comite::find($id);
        $comite->delete();
        return response()->json("deleted ssuccessfuly");
    }
}
