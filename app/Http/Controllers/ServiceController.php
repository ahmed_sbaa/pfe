<?php

namespace App\Http\Controllers;

use App\service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $serv=service::with('citoyens')->orderBy("id")->paginate(3);
        return response()->json($serv);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'etat'=>'required',
            'reponse'=> 'required|min:5',
            'citoyen_id'=> 'required',


        ]);
        $serv = new service();
        $serv->etat=$request->etat;
        $serv->reponse=$request->reponse;
        $serv->citoyen_id=$request->citoyen_id;

        $serv->save();
        return response()->json('service has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $serv=service::find($id);
        return response()->json($serv);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $serv=service::find($id);
        return response()->json($serv);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'etat'=>'required',
            'reponse'=> 'required|min:6',


        ]);
        $serv=service::find($id);

        $serv->update($request->all());
        return response()->json("updated successfuly");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $serv=service::find($id);
        $serv->delete();
        return response()->json("deleted ssuccessfuly");
    }
}
