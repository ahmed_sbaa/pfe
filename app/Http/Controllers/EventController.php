<?php

namespace App\Http\Controllers;

use App\event;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $event=event::with('admins')->orderBy("id")->paginate(3);
        return response()->json($event);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'description'=>'required',
            'date_debut'=> 'required|min:6',
            'date_cloture'=>'required|min:6',
            'cover'=>'required|min:6',


        ]);
        $event = new event();
        $event->description=$request->description;
        $event->date_debut=$request->date_debut;
        $event->date_cloture=$request->date_cloture;
        $event->cover=$request->cover;
        $event->admin_id=$request->admin_id;



        $event->save();
        return response()->json('event has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event=event::find($id);
        return response()->json($event);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event=event::find($id);
        return response()->json($event);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'description'=>'required',
            'date_debut'=> 'required|min:6',
            'date_cloture'=>'required|min:6',
            'cover'=>'required|min:6',


        ]);
        $event=event::find($id);

        $event->update($request->all());
        return response()->json("updated successfuly");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event=event::find($id);
        $event->delete();
        return response()->json("deleted ssuccessfuly");
    }
}
