<?php

namespace App\Http\Controllers;

use App\batir;
use Illuminate\Http\Request;

class BatirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $batir=batir::orderBy("id")->paginate(4);
        return response()->json($batir);
    }
    public function count()
    {
        $batir=batir::all()->count();
        return response()->json($batir);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'plan'=>'required',
            'demande_signe'=> 'required|min:5',
            'titre'=>'required|min:6',
             'projet_construction'=>'required:min:7',
        ]);

        $batir = new batir([
            'plan' => $request->get('plan'),
            'demande_signe'=> $request->get('demande_signe'),
            'titre'=> $request->get('titre'),
            'projet_construction'=> $request->get('projet_construction')

        ]);
        $batir->save();
        return response()->json('batir has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $batir=batir::find($id);
        return response()->json($batir);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $batir=batir::find($id);
        return response()->json($batir);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $batir=batir::find($id);

        $batir->update($request->all());
        return response()->json("updated successfuly");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $batir=batir::find($id);
        $batir->delete();
        return response()->json("deleted ssuccessfuly");
    }
}
