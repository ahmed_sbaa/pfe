<?php

namespace App\Http\Controllers;

use App\batir;
use App\naissance;
use Illuminate\Http\Request;

class NaissanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $naiss=naissance::orderBy("id")->paginate(2);
        return response()->json($naiss);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'prenon'=>'required',
            'nom'=> 'required|min:6',
            'date_naiss'=>'required|min:6',
            'lieu_naiss'=>'required|min:6',
            'prenon_pere'=>'required|min:6',
            'prenon_mere'=>'required|min:6',
            'nom_mere'=>'required|min:6',
            'file'=>'required|min:6',


        ]);
        $naiss = new naissance([
            'prenon' => $request->get('prenon'),
            'nom'=> $request->get('nom'),
            'date_naiss'=> $request->get('date_naiss'),
            'lieu_naiss'=> $request->get('lieu_naiss'),
            'prenon_pere'=> $request->get('prenon_pere'),
            'prenon_mere'=> $request->get('prenon_mere'),
            'nom_mere'=> $request->get('nom_mere'),
            'file'=> $request->get('file')





        ]);
        $naiss->save();
        return response()->json('extraitnaiss has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $naiss=naissance::find($id);
        return response()->json($naiss);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $naiss=naissance::find($id);
        return response()->json($naiss);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'prenon'=>'required',
            'nom'=> 'required|min:6',
            'date_naiss'=>'required|min:6',
            'lieu_naiss'=>'required|min:6',
            'prenon_pere'=>'required|min:6',
            'prenon_mere'=>'required|min:6',
            'nom_mere'=>'required|min:6',
            'file'=>'required|min:6',


        ]);
        $naiss=naissance::find($id);

        $naiss->update($request->all());
        return response()->json("updated successfuly");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $naiss=naissance::find($id);
        $naiss->delete();
        return response()->json("deleted ssuccessfuly");
    }
}
