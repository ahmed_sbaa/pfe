<?php

namespace App\Http\Controllers;

use App\vocation;
use Illuminate\Http\Request;

class VocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vocation=vocation::orderBy("id")->paginate(3);
        return response()->json($vocation);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'demande'=>'required',
            'cin_scan'=>'required',
            'permis_de_batir'=>'required',

        ]);
        $vocation = new vocation([
            'demande' => $request->get('demande'),
            'cin_scan'=> $request->get('cin_scan'),
            'permis_de_batir'=> $request->get('permis_de_batir')



        ]);
        $vocation->save();
        return response()->json('vocation has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vocation=vocation::find($id);
        return response()->json($vocation);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vocation=vocation::find($id);
        return response()->json($vocation);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {

        $this->validate($request,[
            'demande'=>'required',
            'cin_scan'=>'required',
            'permis_de_batir'=>'required',

        ]);
        $vocation=vocation::find($id);

        $vocation->update($request->all());
        return response()->json("updated successfuly");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vocation=vocation::find($id);
        $vocation->delete();
        return response()->json("deleted ssuccessfuly");
    }
}
