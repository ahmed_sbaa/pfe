<?php

namespace App\Http\Controllers;

use App\citoyen;
use App\contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{


    public function test()
    {
        $contact=contact::with('citoyens')->orderBy("id")->paginate(3);
        return response()->json($contact);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contact=contact::with('citoyens','admins')->orderBy("id")->paginate(3);
        return response()->json($contact);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'sujet_message'=>'required|min:6',
            'contenu'=> 'required|min:6|string',
            'email'=>'required|min:6|email',
            'tel'=>'required',
        ]);

        $contact = new contact();
        $contact->contenu=$request->contenu;
        $contact->email=$request->email;
        $contact->sujet_message=$request->sujet_message;
        $contact->tel=$request->tel;

        $contact->citoyen_id=$request->citoyen_id;
        $contact->admin_id=$request->admin_id;
        $contact->save();
        return response()->json('contact has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contact=contact::find($id);
        return response()->json($contact);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contact=contact::find($id);
        return response()->json($contact);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {

        $this->validate($request,[
            'sujet_message'=>'required|min:6',
            'contenu'=> 'required|min:6|string',
            'email'=>'required|min:6|email',
            'tel'=>'required|min:6|number',

        ]);
        $contact=contact::find($id);

        $contact->update($request->all());
        return response()->json("updated successfuly");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact=contact::find($id);
        $contact->delete();
        return response()->json("deleted ssuccessfuly");
    }
}
