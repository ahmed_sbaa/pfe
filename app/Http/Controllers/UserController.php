<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{


    public function getAll(Request $request)
    {
        $search=$request->search;
        $users = User::where('name','LIKE',"%$search%")->get();

        return response()->json($users);
    }
    public function index()
    {
        $users = User::orderBy("id")->paginate(3);

        return response()->json(
            [
                'status' => 'success',
                'users' => $users->toArray()
            ], 200);
    }

    public function show(Request $request, $id)
    {
        $user = User::find($id);

        return response()->json(
            [
                'status' => 'success',
                'user' => $user->toArray()
            ], 200);
    }
    public function count()
    {
        $numberofUsers= User::count();
        return response()->json($numberofUsers);
    }

    public function updateProfile(Request $request ,$id)
    {
        $user = User::find($id);
        if($user){
            $request->validate([
                'name' => 'required|string|between:3,255',

            ]);


            if ($request->avatar) {
                $request->validate([
                    'avatar' => 'mimes:jpeg,bmp,png|required',

                ]);
                $user->avatar = $request->file('avatar')->store('file', 'public');

            }

            if ($request->password || $request->new_password || $request->new_password_confirmation) {
                $request->validate([
                    'password' => ['required', function ($attribute, $value, $fail) use ($user) {
                        if (!\Hash::check($value, $user->password)) {
                            return $fail(__('The current password is incorrect.'));
                        }
                    }],
                    'new_password' => 'required|string|confirmed|different:password|between:8,255',
                ]);

            }
            if ($request->new_password) {
                $user->password = Hash::make($request->new_password);
            }
            $user->name = ucfirst($request->name);
            $user->save();


            return response()->json([
                'message' => __('user_update_success'),

            ]);
        }
        else{
            return response()->json("user not found");
        }

    }

}

