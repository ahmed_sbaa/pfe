<?php

namespace App\Http\Controllers;

use App\reclamation;
use Illuminate\Http\Request;

class ReclamationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $recla=reclamation::with('citoyen')->orderBy("id")->paginate(3);
        return response()->json($recla);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request,[

            'sujet'=> 'required|min:5',
            'contenu'=>'required',
            'file'=> 'required|mimes:jpg,jpeg,png,gif,pdf',


        ]);

        $fileName = null;
        if (request()->hasFile('file')) {
            $file = request()->file('file');
            $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('./images', $fileName);
        }

        $recla = new reclamation();
        $recla->contenu=$request->contenu;
        $recla->sujet=$request->sujet;
        $recla->image=$request->image;
        $recla->file=$fileName;
        $recla->citoyen_id=$request->citoyen_id;

        $recla->save();
        return response()->json('reclamation has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $recla=reclamation::find($id);
        return response()->json($recla);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $recla=reclamation::find($id);
        return response()->json($recla);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'sujet'=> 'required|min:5',
            'contenu'=>'required',
            'file'=> 'required|mimes:jpg,jpeg,png,gif,pdf',

        ]);
        $recla=reclamation::find($id);

        $recla->update($request->all());
        return response()->json("updated successfuly");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $recla=reclamation::find($id);
        $recla->delete();
        return response()->json("deleted ssuccessfuly");
    }
}
