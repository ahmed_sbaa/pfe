<?php

namespace App\Http\Controllers;

use App\citoyen;
use Illuminate\Http\Request;

class CitoyenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $citoyen=citoyen::orderBy("id")->paginate(3);
        return response()->json($citoyen);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'cin_scan'=>'required|number',
            'travail'=> 'required|min:5',


        ]);
        $citoyen = new citoyen([
            'cin_scan' => $request->get('cin_scan'),
            'travail'=> $request->get('travail')


        ]);
        $citoyen->save();
        return response()->json('citoyen has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $citoyen=citoyen::find($id);
        return response()->json($citoyen);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $citoyen=citoyen::find($id);
        return response()->json($citoyen);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'cin_scan'=>'required|number',
            'travail'=> 'required|min:5',


        ]);
        $citoyen=citoyen::find($id);

        $citoyen->update($request->all());
        return response()->json("updated successfuly");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $citoyen=citoyen::find($id);
        $citoyen->delete();
        return response()->json("deleted ssuccessfuly");
    }
}
