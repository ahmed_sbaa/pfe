<?php

namespace App\Http\Controllers;

use App\prop;
use Illuminate\Http\Request;

class ProperieteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prop=prop::orderBy("id")->paginate(3);
        return response()->json($prop);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'demande'=>'required',
            'contract_achat'=> 'required|min:5',
            'cin_scan'=>'required|min:6',

        ]);
        $prop = new prop([
            'demande' => $request->get('demande'),
            'contract_achat'=> $request->get('contract_achat'),
            'cin_scan'=> $request->get('cin_scan')



        ]);
        $prop->save();
        return response()->json('prop has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $prop=prop::find($id);
        return response()->json($prop);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $prop=prop::find($id);
        return response()->json($prop);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {

        $this->validate($request,[
            'demande'=>'required',
            'contract_achat'=> 'required|min:5',
            'cin_scan'=>'required|min:6',

        ]);
        $prop=prop::find($id);

        $prop->update($request->all());
        return response()->json("updated successfuly");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $prop=prop::find($id);
        $prop->delete();
        return response()->json("deleted ssuccessfuly");
    }
}
