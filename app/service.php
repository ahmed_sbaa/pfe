<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class service extends Model
{
    protected $fillable = [
        'etat', 'reponse',
    ];
    public function citoyens(){
        return $this->belongsTo(citoyen::class,'citoyen_id');
    }
}
