<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class event extends Model
{
    protected $fillable = [
        'description', 'date_debut', 'date_cloture','cover','admin_id',
    ];
    public function admins(){
        return $this->belongsTo(admin::class,'admin_id');
    }
}
