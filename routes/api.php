<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::get('/users', 'UserController@getAll');
Route::resource('/actualite', 'ActualiteController');
Route::resource('/agent', 'AgentController');
Route::resource('/batir', 'BatirController');
Route::resource('/citoyen', 'CitoyenController');
Route::resource('/contact', 'ContactController');
Route::resource('/event', 'EventController');
Route::resource('/naiss', 'NaissanceController');
Route::resource('/Prop', 'ProperieteController');
Route::resource('/reclam', 'ReclamationController');
Route::resource('/service', 'ServiceController');
Route::resource('/sondage', 'StatController');

Route::resource('/vocation', 'VocationController');
Route::resource('/comite', 'ComiteController');
Route::get('/usser', 'UserController@getAll');

Route::post('updatep/{id}', 'ActualiteController@update');

Route::get('/counte', 'BatirController@count');
Route::get('/counte2', 'ActualiteController@count');


//Route::prefix('auth')->group(function () {
//    Route::post('register', 'AuthController@register');
//  Route::post('login', 'AuthController@login');
//    Route::get('refresh', 'AuthController@refresh');
//
//    Route::group(['middleware' => 'auth:api'], function(){
//        Route::get('user', 'AuthController@user');
//        Route::post('logout', 'AuthController@logout');
//    });
//});
//Route::group(['middleware' => 'auth:api'], function(){
//    // Users
//    Route::get('users', 'UserController@index')->middleware('isAdmin');
//    Route::get('users/{id}', 'UserController@show')->middleware('isAdminOrSelf');
//});

Route::group([
    'prefix' => 'auth',
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
    Route::post('userProfile/{id}', 'UserController@updateProfile');
    Route::get('usersh/{id}', 'UserController@show');

    Route::group([
        'middleware' => 'auth:api',
    ], function () {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});
